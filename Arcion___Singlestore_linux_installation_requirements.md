# Table of Contents

1. [System requirements](#1-system-requirements)
    * 1.1 [Hardware Requirements](#11-hardware-requirements)
    * 1.2 [Operating System Requirements](#12-operating-system-requirements)
2. [Pre-requisite](#2-pre-requisite)
    * 2.1 [Create memsql user](#21-create-memsql-user)
    * 2.2 [Configure Passwordless SSH](#22-configure-passwordless-ssh)
3. [Ports and Mounts](#3-ports-and-mounts)
4. [References](#10-references)


# 1. System Requirements

## 1.1 Hardware Requirements:

#### CPU
* For each host, an x86_64 CPU and a minimum of 4 cores (8 vCPUs)

* SingleStoreDB is optimized for architectures supporting SSE4.2 and AVX2 instruction set extensions, but will run successfully on x86_64 systems without these extensions.

#### Memory
* A minimum of 8GB of RAM available for each node

* A minimum of 32GB of RAM available for each leaf node

* It is strongly recommended to run leaf nodes on hosts that have the same hardware and software specifications.

#### Storage
- Never more than 60% utilized.
- 30 MB/s per physical core.
- 300 IOPS per core
- For 16-Core host disk should capable of 500 MB/s and 5000 IOPS.
- Rowstore capacity is limited by the capacity of RAM.
- For rowstore the amount of capacity needed is 5x of RAM.
- For columnstore, the amount of disk space required is the size of raw data.

- For high availability(HA) the amount of storage required is 2x size of raw data.


## 1.2 Operating System Requirements:
**Platform and Kernel**

* Red Hat Enterprise Linux (RHEL) / AlmaLinux

* Debian 8 or 9 (version 9 is preferred)

* Minimum Linux kernel version required is 3.10 or later.



## Configuration Settings

### 1. Configure File Descriptor and Maximum Process Limits

* A SingleStoreDBcluster uses a substantial number of client and server connections between aggregators and leaf nodes to run queries and cluster operations.

* Modify the /etc/security/limits.conf file and set the maximum nice limit to -10 on each Linux host in the cluster. This will allow the SingleStoreDB engine to run some threads at higher priority, such as the garbage collection threads.

* The maximum process limit for these connections are listed below:

```
memsql -       nice    -10
memsql   soft   NOFILE   1024000
memsql   hard   NOFILE   1024000
memsql   soft   nproc    128000
memsql   hard   nproc    128000
```
* This ulimit settings can be configured in the `/etc/security/limits.conf` file, or directly via shell commands.

Note : Each node must be restarted for the changed ulimit settings to take effect.
* The `file-max` setting configures the maximum number of file handles (file descriptor limit) for the entire system.
* the `file-max` value must be higher than the `NOFILE` setting.  
* Increase the maximum number of file handles configured for the entire system in `/proc/sys/fs/file-max`
* For permenant system modification append or modify the fs.`file-max` line in the `/etc/sysctl.conf` file.
  ```
  Vi /etc/sysctl.conf
  ```
* Set the value of `fs.file-max` minimum `1024001` or greater.
  ```
  fs.file-max = 1024001
  ```
* To persist the update run.
  ```
  sudo sysctl -p /etc/sysctl.conf
  ```

### 2. Configure Transparent Huge Pages
Transparent Huge Pages (THP) is a Linux memory management system that reduces the overhead of Translation Lookaside Buffer (TLB) lookups on machines with large amounts of memory by using larger memory pages.

* Disable THP at boot time on all nodes
* THP lag may result in inconsistent query run times or high system CPU

To disable THP, add the following lines to the end of /etc/rc.local before the exit line (if present), and reboot the host:
```
echo never > /sys/kernel/mm/transparent_hugepage/enabled
echo never > /sys/kernel/mm/transparent_hugepage/defrag
echo 0 > /sys/kernel/mm/transparent_hugepage/khugepaged/defrag
echo no > /sys/kernel/mm/transparent_hugepage/khugepaged/defrag
echo never > /sys/kernel/mm/redhat_transparent_hugepage/enabled
echo never > /sys/kernel/mm/redhat_transparent_hugepage/defrag
echo 0 > /sys/kernel/mm/redhat_transparent_hugepage/khugepaged/defrag
echo no > /sys/kernel/mm/redhat_transparent_hugepage/khugepaged/defrag
```
* On Red Hat / CentOS distributions, THP will be under redhat_transparent_hugepage.
* The khugepaged/defrag option will be 1 or 0 on newer Linux versions.
* The khugepaged/defrag option will be 1 or 0 on newer Linux versions.
* cat /sys/kernel/mm/*transparent_hugepage/khugepaged/defrag. Keep only the matching setting. For example, if you see 1 or 0.
* Check which your system uses by running `cat /sys/kernel/mm/*transparent_hugepage/khugepaged/defrag`.
* If you see 1 or 0, keep the line with `echo 0`; if you see yes or no, keep the line with `echo no.`


### 3. Configure the Linux nice Setting

* modify the `/etc/security/limits.conf` file and set the maximum nice limit to `-10` on each Linux host in the cluster.

* Nice limit allows singlestore engine to work on high priority threads such as garbage collection.

### 4. Configure Linux ulimit Settings
* The limitations for per-user usage of system resources such as threads, files and network are called ulimits.
* The ulimit settings can be configured in the `/etc/security/limits.conf` file, in the `/etc/security/limits.d` flle, or directly via shell commands.

### 5. Configure Linux vm Settings
SingleStore recommends letting first-party tools, such as `sdb-admin` and `memsqlctl`, configure your vm settings to minimize the likelihood of getting memory errors.

| Variable | Configuration |
| ------ | ------ |
|     vm.max_map_count   |   1000000000     |
|      vm.overcommit_memory  |    0 (>0 recommended only for systems with swap areas larger then physical memory)    |
|vm.overcommit_ratio| 99 (If vm.overcommit_memory is set to 2)|
|vm.min_free_kbytes | 1% of RAM or 4GB which ever is samller|
|vm.swappiness| 1 - 10|


**Note:** Perform the following steps on each host in the cluster.
1. SSH to Node where you want to change these variables.
2. As root, display the current `sysctl` settings and confirm the values of above mentioned `vm.` variables are configured as per the table above.
    ```
    sudo sysctl -a | grep 'vm.'
    ```
3. If the values are not configured as per the table above, As Root Add the following lines to the end of the `/etc/sysctl.conf` file.
    ```
    sudo vi /etc/sysctl.conf
    ```
    ```
    vm.max_map_count=1000000000
    vm.overcommit_memory=0
    vm.overcommit_ratio=99
    vm.min_free_kbytes=658096
    vm.swappiness=1

    ```
    or you can set the values manually using the `/sbin/sysctl command`

    * `sudo sysctl -w vm.max_map_count=1000000000`

    * `sudo sysctl -w vm.overcommit_memory=0`

    * `sudo sysctl -w vm.overcommit_ratio=99`

    * `sudo sysctl -w vm.min_free_kbytes=658096`

    * `sudo sysctl -w vm.swappiness=1`

4. Persist these updates across reboots.
    * `sudo sysctl -p /etc/sysctl.conf`

### 6. Configure Swap Space
* It is recommended that you create a swap partition (or swap file on a dedicated device) to serve as an emergency backing store for RAM.

*  SingleStoreDB makes extensive use of RAM (especially with rowstore tables), so it is important that the operating system does not immediately start killing processes if SingleStoreDB runs out of memory.

* Because typical hosts running SingleStoreDB have a large amount of RAM (> 32 GB/node), the swap space can not be small (>= 10% of physical RAM).

  Ex:VM having RAM:32 GB (Master VM), swap Space should be more than 3.2 GB for VM. Keep Swap Size = 8 Gb   
  If VM RAm= 64 GB (Leaf Node), then Swap Size can kept above 6.4Gb. Keep Swap Size = 8 Gb    

### 7. Configure Network Time Protocol Service
* Install and run ntpd to ensure that system time is in sync across all nodes in the cluster.

  For Red Hat/CentOS distributions:

  `sudo yum install ntp`

### 8. Configure Network Settings
Note: Perform the following steps on each host in the cluster.

1. As root, display the current `sysctl` settings and review the values of `rmem_max` and `wmem_max`.
  
   * `sysctl -a | grep mem_max`
2. Confirm that the receive buffer size (rmem_max) is 8 MB for all connection types. If not, add the following line to the `/etc/sysctl.conf` file.
   * `net.core.rmem_max = 8388608`
3. Confirm that the send buffer size (wmem_max) is 8 MB for all connection types. If not, add the following line to the `/etc/sysctl.conf` file.
   * `net.core.wmem_max = 8388608`

4. To avoid network timeouts, net.core.somaxconn should be at least 1024.
   *  `net.core.somaxconn = 1024`

5. Persist these updates across reboots.
   * `sysctl -p /etc/sysctl.conf`


### 9. Toolbox Checkers
**CPU Checkers**

|Host Setting|Checker|Configuration|
|------------|-------|-----------|
|CPU Features|`cpuFeatures`  Check that the flags field contains the sse4_2 or avx2 extensions set.|Recommended: sse4_2 and  avx2 are enabled|
|CPU Frequency|`cpuFreqInfo` Collects information about CPU frequency configuration.| The following folders should exist:`/sys/devices/system/cpu/cpufreq` and `/sys/devices/system/cpu`|
 |CPU Hyperthreading|`cpuHyperThreading` Checks that hyperthreading is enabled on each host via the lscpu command|Hyperthreading is enabled|
|CPU Threading Configuration|`cpuThreadingInfo` 	Collects information about CPU threading configuration|Hyperthreading is enabled if the number of threads per core > 1|
|CPU Idle and Utilization|`cpuIdle`Checks the CPU utilization & idle time|> 25%: Recommended, < 25%: Use host with caution, < 5%: Host not recommended|
|CPU and Memory Bandwidth|`cpuMemoryBandwidth` Check that CPU and memory bandwidth is appropriate|4 GB/s per CPU and memory leatency should be 500000ns max|
|CPU Model|`CPU Model`- Checks CPU model | Same on all host|
|CPU Power Control|`cpuFreqPolicy` Checks that power saving and turbo mode settings| Turbo and powerSave are disabled|


**Memory Checkers**

|Host Setting|Checker|Configuration|
|------------|-------|------------------|
|Committed Memory|`memoryCommitted` Checks the committed memory on each host and determines if it’s acceptable|< 30% : Recommended, 70-90%:  Caution, 90%: Host not recommended|
|Maximum Memory Settings|`maxMemorySettings` Checks the host’s maximum memory settings| Total `maximum_memory`(57.6 GB)  is 90% of host total memory(64 GB), `maximum_table_memory`(51.84) < 91% of Total `maximum_memory`|

**Disk Checkers**
| Host Setting | Checker | Configuration |
| ------ | ------ | ------ |
|   Presence of an SSD     |`validateSsd` -Verifies if a host is using an SSD        | Recommended SSD Storage|
|     Disk Bandwidth   | `diskBandwidth` - Checks the disk band width    | Read/ Write Speed >= 128 operations/second|
|Disk Latency| `diskLatency` with `diskLatencyRead`,`diskLatencyWrite` - Checks read write latency on disk| <10 ms - Recommended, > 10 ms - Caution, > 25 ms - Not recommended|
|Disk Storage in Use|`diskUsage` - Checks the amount of freee diskspace| > 30% - Recommended, 70% - 80% - Caution, > 80% - Not Recommended|

### 10. Configure Hyperthreading
Hyper-Threading is a proprietary implementation of Simultaneous MultiThreading (SMT) from Intel. The chip-maker originally called it Hyper-Threading Technology, so we’ll shorten it as HTT. It’s present in modern processors from the manufacturer from 2002 onwards.

The aim of HTT, or in general any SMT technology, is to improve the parallelization of tasks. It achieves this by allowing multiple and independent tasks (threads of execution) to share some of the resources of the processors.

In Linux, this is represented as virtual cores, also known as logical CPUs, that are coupled with physical CPUs. In the remainder of the article, we’ll refer to HTT, but everything applies to SMT as well.

##### There are two techniques we can use to check if our system’s using HTT:
  1. We can call lscpu and just search (with grep) on its output the number of threads that each core has:
  
    $ lscpu | grep "Thread(s) per core
    Thread(s) per core: 2
  From this output, we know that HTT is enabled because there’s more than one thread per core (two in this case). In case we had only one thread per core, we would know that HTT is disabled.

  2. We can inspect the file that contains the flag with the activation mode of SMT:

    $ cat /sys/devices/system/cpu/smt/active
    1
  A value of 1 indicates that we have HTT enabled, while a 0 indicates that we have it disabled.

##### To Enable Hyperthreading
  - Run
    `echo 1 > /sys/devices/system/cpu/smt/control`

# 2. Pre-requisite:

## 2.1 Create memsql user:
  * There should be No memsql folder in home directory of memsql user because it will automatically created by singlestore.
  * “memsql” user must be created with “memsql” group on all host machines.
Using Command:
    ```
    useradd memsql
    ```
    **Note:** Group will be automatically get created with the same name as username used while creating useradd command like 'memsql' name group will be created after using above command.

## 2.2 Configure Passwordless SSH:
  * Either SSH keys or password should be there for all memsql user.
  * Passwordless SSH access to memsql user of all host should be there.

  * Passwordless SSH Should be configured such that SSH can be performed from Master Aggregator(MA) to all Leaf Nodes and Child Aggregator(CA).

  * If Child Aggregator(CA) is present in Singlestore (SS) Cluster,then CA should able to connect with MA and all Leaf nodes in Singlestore Cluster.

    Refrence:[Passwordless SSH](https://www.strongdm.com/blog/ssh-passwordless-login#:~:text=SSH%20passwordless%20login%20is%20an,the%20private%20key%20can%20connect)


    OS Requirements Ref link:
[OS Requirements](https://docs.singlestore.com/db/v8.0/en/reference/configuration-reference/cluster-configuration/system-requirements-and-recommendations.html)



### This configuration is applied when following error/warnings occur during installation.
**ERROR MSG:**
```
FAIL /sys/kernel/mm/transparent_hugepage/enabled is [always] on <ip_addr> 
FAIL /sys/kernel/mm/transparent_hugepage/defrag is [always] on <ip_addr>
```
**Transparent Huge Pages Configuration**

**Solution:**  **Perform configuration settings Step 2.**
* Linux organizes RAM into pages that are usually 4 KB in size. Using transparent huge pages (THP), Linux can instead use 2 MB pages or larger. As a background process, THP transparently re-organizes memory used by a process inside the kernel by either merging small pages to huge pages or splitting few huge pages to small pages. This may block memory usage on the memory manager, which may span for a duration of a few seconds, and prevent the process from accessing memory.
* As SingleStore DB uses a lot of memory, we recommend that you disable THP at boot time on all nodes (master aggregator, child aggregators, and leaf nodes) in the cluster. THP lag may result in inconsistent query run times or high system CPU (also known as red CPU).
* To disable THP, add the following lines to the end of /etc/rc.local before the exit line (if present), and reboot the host:


# 3. Ports and Mounts

| Protocol  | Port  | Direction       | Description |
| --------  | ----  | ---------       | ----------- |
| TCP       |  22   | In and Outbound | For host access. Required between nodes in SingleStore DB tool deployment scenarios. Also useful for remote administration and troubleshooting on the main deployment host.|
| TCP       | 443   | Outbound        | To get public repo key for package verification. Required for nodes downloading SingleStore APT or YUM packages. |
| TCP       | 3306  | In and Outbound | Default port used by SingleStore DB. Required on all nodes for intra-cluster communication. Also required on aggregators for client connections. |
| TCP       | 8080  | In and Outbound | Default port for Master and Child Aggregator SingleStore DB Studio. (Only required for the host running Studio.) |

**Note:** To check any port is already in use or not use command 'sudo netstat -anpe | grep "port" | grep "LISTEN"'
For E.g. In this below example we can see port 3306 is being used by memsqld service.
```
$ sudo netstat -anpe | grep "3306" | grep "LISTEN"
tcp        0      0 0.0.0.0:3306            0.0.0.0:*               LISTEN      1001       1370166    65795/memsqld       
tcp6       0      0 :::33060                :::*                    LISTEN      27         6687903    364561/mysqld
```

# 10. References

Installation: https://docs.singlestore.com/db/v8.1/en/deploy/linux/yaml-offline-tar.html

Client and driver Download
https://docs.singlestore.com/managed-service/en/connect-to-your-cluster/client-and-driver-downloads.html



